
// 截图
function rootGetScreen() {
    // shell("screencap -p /sdcard/code_all.png", true)
    toastLog('开始截图')
    var img = captureScreen();
    images.saveImage(img, "/sdcard/code_all.png");
    toastLog('截图成功')
}

//获取验证码图片
function get_code_img(){
    toastLog('开始截图')
    log('开始截图')
    rootGetScreen()
    sleep(2000)
    var src = images.read("/sdcard/code_all.png");
    var localtion = className("android.widget.Image").findOne(10*1000).bounds()
    var clip = images.clip(src, localtion.left, localtion.top, localtion.width(), localtion.height());
    images.save(clip, "/sdcard/code.jpg", "jpg", 50);
    src.recycle()
    clip.recycle()
    log('获取图片成功')
    toastLog('获取图片成功');
}

//超级鹰模块
//////////////////////////////////////////////////////////////////////////////////////////////

//超级鹰上报
function report_error_cjy(username, password, softid, img_id){
    http.__okhttp__.setTimeout(3e4);
    i = device.release, c = device.model, s = device.buildId;
    try {
        var n = http.postJson(
            "http://upload.chaojiying.net/Upload/ReportError.php",
            {
                user: username,
                pass: password,
                softid: softid,
                id:img_id
            }, 
            {
                headers: {
                "User-Agent": "Mozilla/5.0 (Linux; Android " + i + "; " + c + " Build/" + s + "; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/66.0.3359.126 Mobile Safari/537.36",
            }
        });
    } catch (e) {
        return false;
    }
    var d = n.body.json(), p = d.code, m = d.message;
    log(d)
    return d
}

//图片的base64
function img64(imgFile){
    let img=images.read(imgFile)
    let img64=images.toBase64(img)
    return img64
}

//获取验证码
function get_tp_code(){
    var res = http.post("http://upload.chaojiying.net/Upload/Processing.php", 
    {
        user: tp_user,
        pass: tp_pwd,
        softid: softid,
        codetype: '5201',
        file_base64: img64(img)
    }
    );
    var result = JSON.parse(res.body.string())
    if(result['err_no'] == 0){
    return [result["pic_id"], result["pic_str"]]
    }
    log(result)
    return false 
}

// 输入验证码
function my_captcha() {
    // 获取验证码图片
    log('开始打码')
    get_code_img()
    // 获取验证码
    var code = false
    for (let index = 0; index < 10; index++) {
        code = get_tp_code()
        if (code) {
            break
        }else{
            sleep(10*000)
        }
    }
    if(!code){
        return false
    }
    toastLog('获取到的验证码:' + code[1])
    //  输入验证码
    log('输入验证码')
    setText(code[1])
    sleep(5000);

    // 确认输入验证码
    log('确认验证码')
    click('确定')
    sleep(5000);
    if (text('安全验证').exists()) {
        report_error_cjy(tp_user, tp_pwd, softid, code[0])
    }
}

// 判断验证码成功否
function captcha_anormal() {
    sleep(15000)
    var zz = 0
    while(true){
        if (text('安全验证').exists()) {
            toastLog('检测到验证码')
            my_captcha()
        }else{
            sleep(10*1000)
            if(zz % 10 == 0){
                log('打码线程检测中')
            }
        }
        zz += 1
    }
}


//////////////////////////////////////////////////////////////////////////////////////////////


function Sign_in() {
    // 签到
    text('领音符').findOnce()
    click('领音符')
    sleep(2000)
    text('签到').findOnce()
    click('签到')
    sleep(2000)
    text('立即签到得音符').findOnce()
    click('立即签到得音符')
    sleep(2000)
    click('看广告视频+288音符')
    // 看广告时间
    while (true) {
        sleep(5000)
        if (text('签到').visibleToUser(true).findOnce() == null) {
            back()
        }else{
            break;
        }
    }
    sleep(2000)
    click('主页')
}

function Get_notes() {
    // 刷领取音符
    for (let index = 0; index < 36; index++) {
        click('领取音符')
        toast('已经刷了'+index+'个了')
        sleep(2000)
        // 关闭广告
        while (true) {
            sleep(5000)
            if (text('领取音符').visibleToUser(true).findOnce() == null) {
                back()
                }else{
                    break;
                }
        }
        sleep(2000)
    }
}

function Slide_video() {
    // 刷视频
    num = 0
    while (true) {
        // 关闭广告
        while (true) {
            sleep(5000)
            if (text('领取音符').visibleToUser(true).findOnce() == null) {
                back()
            }else{
                break;
            }
        }
        sleep(2000)
        scrollDown()
        sleep(2000)
        // 刷到广告情况
        if (text('启动').exists()||text('浏览').exists()||text('下载').exists()) {
            if (text('已领').exists()) {
                continue;
            }else{
                sleep(20000)
                if (className("android.widget.TextView").text("领取").findOnce() != null) {
                    className("android.widget.TextView").text("领取").findOne().parent().click()
                    toast('领取~')
                }
                // log('领取~')
                sleep(5000)
                continue;
            }
        }
        sleep(random(25000, 30000))
        num++
        // log('已经刷了'+num+'个视频')
        toast('已经刷了'+num+'个视频')
    }
}

function start(a, b, c) {
    toast('检测无障碍服务')
    auto.waitFor();  // 无障碍服务是否已经启用
    sleep(3000)
    toast('请勾选始终允许')
    if(!requestScreenCapture()){
        toast("请求截图失败");
        exit();
    }
    sleep(3000)
    toast('请于15秒内打开凹音极速版')
    sleep(15000)
    // 开启检测验证码线程
    threads.start(captcha_anormal);
    if (a) {
        // 签到
        Sign_in()
    }
    if (b) {
        // 刷领取音符
        Get_notes()
    }
    if (c) {
        // 刷视频
        Slide_video()
    }
}


storage = storages.create("ayjsb");

a = storage.get("Sign_in");
b = storage.get("Get_notes");
c = storage.get("Slide_video");

// 打码账号
var tp_user = storage.get('tp_user');
// 打码密码
var tp_pwd = storage.get('tp_pwd');
// 打码id
var softid = storage.get('softid');
var softid = 910292
// 图片保存地址
var img = '/sdcard/code.jpg'

start(a, b, c)


// click('领取音符')
// 关闭广告 className("android.widget.ImageView").findOne().click()
// swipe(device.width / 2, device.height - 200, device.width /2, 100, 500)
// click('领取')
// log(className("android.widget.ImageView").findOne().indexInParent())

/*
if (text('安全验证').exists()) {
    log('检测到验证码')
    exit()
}
*/
// log(text('安全验证').findOne())
// log(text('确定').findOne())
// log(text('不区分大小写，点击可更换验证码').exists())
// 验证码图片
//a = className("android.widget.ImageView").visibleToUser(true).clickable(false).find()
//log(a.length)
//log(a[3])
