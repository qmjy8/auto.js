"ui";
var storage = storages.create("ayjsb");

// 打码账号
var tp_user = storage.get('tp_user');
if (tp_user == undefined) {
    tp_user = '请输入打码账号'
}
// 打码密码
var tp_pwd = storage.get('tp_pwd');

if (tp_pwd == undefined) {
    tp_pwd = '请输入打码密码'
}
// 打码id
var softid = storage.get('softid');
if (softid == undefined) {
    softid = '请输入打码id'
}

xml ='<vertical padding="16">\
        <button id="ok" text="开始"/>\
        <text text="音量上键停止运行" textSize="40sp"  gravity="center"/>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="打码账号"/>\
        <input id="tp_user" text="' + tp_user + '"/>\
        </horizontal>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="打码密码"/>\
        <input id="tp_pwd" text="' + tp_pwd + '"/>\
        </horizontal>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="打码id"/>\
        <input id="softid" text="' + softid + '"/>\
        </horizontal>\
        <horizontal>\
        <checkbox id="Sign_in" checked="true" text="签到"/>\
        </horizontal>\
        <horizontal>\
        <checkbox id="Get_notes" checked="true" text="刷领取音符"/>\
        </horizontal>\
        <horizontal>\
        <checkbox id="Slide_video" checked="true" text="刷视频"/>\
        </horizontal>\
    </vertical>'


ui.layout(
    xml
);

ui.Sign_in.on("check", (checked)=>{
    if(checked){
        toast("签到被勾选了");
    }else{
        toast("签到被取消勾选了");
    }
});

ui.Get_notes.on("check", (checked)=>{
    if(checked){
        toast("刷领取音符被勾选了");
    }else{
        toast("刷领取音符被取消勾选了");
    }
});

ui.Slide_video.on("check", (checked)=>{
    if(checked){
        toast("刷视频被勾选了");
    }else{
        toast("刷视频被取消勾选了");
    }
});

// 指定确定按钮点击时要执行的动作
ui.ok.click(function(){
    // 通过getText()获取输入的内容
    // toast("开始辣");
    var Sign_in = ui.Sign_in.checked;
    var Get_notes = ui.Get_notes.checked;
    var Slide_video = ui.Slide_video.checked;
    var tp_user = ui.tp_user.text();
    var tp_pwd = ui.tp_pwd.text();
    var softid = ui.softid.text();

    storage.put("Sign_in", Sign_in);
    storage.put("Get_notes", Get_notes);
    storage.put("Slide_video", Slide_video);
    storage.put("tp_user", tp_user);
    storage.put("tp_pwd", tp_pwd);
    storage.put("softid", softid);
    
    var path = 'ayjsb.js'
    execution = engines.execScriptFile(path);
});
