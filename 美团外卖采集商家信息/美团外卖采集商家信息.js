
var now = new Date();
var end = new Date('2020-7-30');
if(now > end){
    exit();
}

function r_tap(uiobj){
    try {
        //toast([uiobj.bounds().centerX(), uiobj.bounds().centerY()])
        //Tap(uiobj.bounds().centerX(), uiobj.bounds().centerY());
        click(uiobj.bounds().centerX(), uiobj.bounds().centerY());
    } catch (error) {
        log(error)
    }
}

//获取当前页面的店铺
function get_shop_list(){
    var shops = [];
    var shop_list = id('com.sankuai.meituan.takeoutnew:id/textview_poi_name').find();
    shop_list.forEach(element => {
        shops.push(element.text())
    });
    return shops
}

//翻页
function next_page(){
    if(id('com.sankuai.meituan.takeoutnew:id/recycler_view').exists()){
        id('com.sankuai.meituan.takeoutnew:id/recycler_view').findOne().scrollForward()
        sleep(1000);
    }
    swipe(device.width * 0.6, device.height * 0.8, device.width * 0.6, device.height * 0.6, 1000)
    if(text('加载更多').exists()){
        // swipe(360, 540, 360,300, 1)
        swipe(device.width * 0.6, device.height * 0.8, device.width * 0.6, device.height * 0.6, 1000)
        sleep(5000);
        if (text('加载更多').findOnce() != null) {
            // text('加载更多').findOnce().click()
            r_tap(text('加载更多').findOnce())
        }
    }
}

//检查底部
function end_check(){
    for (let index = 0; index < 5; index++) {
        if(text('更多商家接入中，敬请期待').exists()){
            // swipe(360, 540, 360,300, 1)
            swipe(device.width * 0.6, device.height * 0.8, device.width * 0.6, device.height * 0.6, 1000)
            sleep(9000);
        }else{
            return false
        }
    }
    if(text('更多商家接入中，敬请期待').exists()){
        return true
    }
    return false
}

//初始化
function init(){
    if (id('com.sankuai.meituan.takeoutnew:id/textview_poi_name').exists()){
        return true
    }
    for (let index = 0; index < 5; index++) {
        id('com.sankuai.meituan.takeoutnew:id/nested_scroll_recycler_view').findOne().scrollForward();
    }
    if (text('全部筛选').exists()){
        if(!text('销量优先')){
            text('综合排序').findOne().parent().click();
            text('销量优先').findOne().parent().click();
            sleep(2000);
        }
        return true 
    }
    return false
}

//获取商家电话号码
function get_phone(shop_name){
    var phone;
    var normal = false;
    text(shop_name).findOne().parent().parent().click();
    sleep(3000);
    /*
    if (textStartsWith('本店已打烊') && textStartsWith('本店休息啦')) {
        back()
    }
    
    if(!id('com.sankuai.meituan.takeoutnew:id/back_item').exists()){
        toastLog('没有打开');
        back()
        sleep(3000)
        back()

        return "error";
    }
    */
    for (let index = 0; index < 7; index++) {
        if(!text('商家').visibleToUser(true).exists()){
            back()
            sleep(5000);
        }else{
            break
        }
    }    

    for (let index = 0; index < 8; index++) {
        if(text('商家').exists()){
            normal = true
            break
        }else{
            sleep(1000)
        }
    }
    if (normal){
        text('商家').findOne().parent().click();
        sleep(3000);
        address = id('com.sankuai.meituan.takeoutnew:id/txt_poi_address').findOne().text()
        id('com.sankuai.meituan.takeoutnew:id/view_poi_phone').findOne().click();
        sleep(3000);
        phone = className('android.widget.TextView').find()[1].text();
        phone = address + ',' + phone
        text('取消').findOne().click();
    }else{
        toastLog('没有电话');
        phone = "none"
    }
    // id('com.sankuai.meituan.takeoutnew:id/back_item').findOne().click()
    for (let index = 0; index < 7; index++) {
        if (!text('搜索').exists()) {
            back()
            sleep(5000);
        }
    }
    return phone;
}

//检查当前app
function check_app(app_name){
    while(true){
        if (currentPackage() == app_name){
            return true
        }else{
            toast('当前app不是目标app,请打开美团');
            sleep(3000);
        }
    }
}

function save_data(file_name, data){
    files.append(file_name, data);
}

var app_name = 'com.sankuai.meituan.takeoutnew';

var done_list;
var temp_list;
var phone;
var address;
var shop_name;
var storage = storages.create('MT');
done_list = storage.get('done_list') 
if (done_list == undefined){
    done_list = [];
}

function my_start() {
    record_time = new Date().getTime();
    while(true){
        // 初始化
        init();
        // 获取商家列表
        temp_list = get_shop_list();
        log(temp_list);
        for (let index = 0; index < temp_list.length; index++) {
            record_time = new Date().getTime();
            shop_name = temp_list[index];
            if(done_list.indexOf(shop_name) >= 0){
                log('跳过:'+shop_name);
                continue
            }
            log('打开:'+shop_name);
            if (!text(shop_name).exists()) {
                next_page()
            }
            record_time = new Date().getTime();
            phone = get_phone(shop_name);
            log('地址-手机:' + phone);
            information = shop_name + "," + phone+"\n"
            //写入文件
            files.append("/sdcard/商家信息.csv", information);
            //save_data('shop.csv', shop_name+","+phone+"\n")
            done_list.push(shop_name);
            storage.put('done_list', done_list);
        }
        record_time = new Date().getTime();
        if (end_check()){
            toastLog('检查到底部，任务结束')
            break
        }
        next_page();
    }
}

toastLog('开始操作')
//check_app(app_name)



auto.waitFor();  // 无障碍服务是否已经启用
console.show()  // 控制台
storage = storages.create("meituan");
my_number = 0


while (true) {
    
    state = storage.get("state");
    // log(String(state)+'当前状态')
    if (state==1 && my_number == 0) {
        my_number++
        threads.start(my_start);
       log('开启线程')
    }
    else if(state == -1){
        state = 0
        my_number = 0
        exit()
    }
    sleep(1000);
    // 防卡死
    if (state==1) {
        if (new Date().getTime()-record_time > 120000) {
            threads.shutDownAll()
            for (let index = 0; index < 7; index++) {
                if (!text('搜索').exists()) {
                    back()
                    sleep(5000);
                }
            }
            my_number = 0
            sleep(3000)
        }
    }    
}
