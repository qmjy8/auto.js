
/**如果弹出青少年窗口，点击 */
function youngWin() {
    while (true) {
        if (text("进入青少年模式").exists()) {
            youngWin = text("确定")
            console.log("点击了确定(青少年窗口)");
            youngWin.click();
        };
    }
}

function judge_home() {
    // 是否进入首页
    for (let index = 0; index < 7; index++) {
        if (!className("android.widget.TextView").text('首页').selected(true).exists()) {
            back()
            sleep(3000)
        }else if(className("android.widget.TextView").text('首页').selected(true).exists()){
            if (textContains("发现").className("android.widget.TextView").exists()) {
                textContains("发现").className("android.widget.TextView").click()
                sleep(3000)
            }else{
                r_tap(className("android.widget.TextView").text('首页').selected(true))
                sleep(3000)
            }
            return;
        }
        sleep(3000)
    }
    kill("com.meitu.meipaimv")
    sleep(3000)
    init()
}

function kill(packageName){
    log('杀掉进程')
    // 关闭app
    for (let index = 0; index < 6; index++) {
        if (currentPackage() == packageName){
            back();
            log(currentPackage())
            if(currentPackage() != packageName){
                return true
            }
            sleep(300)
        }
    }
}

function r_tap(uiobj){
    // 点击函数
    try {
        //toast([uiobj.bounds().centerX(), uiobj.bounds().centerY()])
        //Tap(uiobj.bounds().centerX(), uiobj.bounds().centerY());
        click(uiobj.bounds().centerX(), uiobj.bounds().centerY());
    } catch (error) {
        log(error)
    }
}

function like() {
    log('点赞')
    // 点赞
    if (className('android.widget.ImageView').id('anv').boundsInside(0, device.height * 0.1, device.width, device.height * 0.9).exists()) {
        if (!className('android.widget.ImageView').id('anx').exists()) {
            let btn = className('android.widget.ImageView').id('anv').boundsInside(0, device.height * 0.1, device.width, device.height * 0.9).findOnce()
            if (btn){
                r_tap(btn)
            } 
        }
    } else {
        return;
    }
}

function care() {
    log('关注')
    // 关注
    if (className('android.view.View').id('kr').clickable(true).boundsInside(0, device.height * 0.1, device.width, device.height * 0.9).exists()) {
        let btn = className('android.view.View').id('kr').clickable(true).boundsInside(0, device.height * 0.1, device.width, device.height * 0.9).findOnce()
        if (btn) {
            btn.click()
        }
    } else {
        return;
    }
}

function init() {
    log('启动美拍')
    // 启动美拍
    app.startActivity({
        action: "android.intent.action.MAIN",
        packageName: "com.meitu.meipaimv",
        className: "com.meitu.meipaimv.StartupActivity",
        category: ["android.intent.category.LAUNCHER"],
        flags: ["activity_new_task"]
    });
    // 是否进入首页
    judge_home()
    // 进入视频
    if (className("androidx.recyclerview.widget.RecyclerView").scrollable(true).depth(14).exists() && className("android.widget.ImageView").depth(17).exists()) {
        icon = className("androidx.recyclerview.widget.RecyclerView").scrollable(true).depth(14).findOne().children().findOne(className("android.widget.ImageView").depth(17))
        r_tap(icon)
    }else{
        kill("com.meitu.meipaimv")
        sleep(3000)
        init()
    }
}

function like_threads() {
    
    // 点赞线程
    while (true) {
        sleep(random(like_min, like_max)*1000)
        like()
    }
}

function care_threads() {
    // 关注线程
    while (true) {
        sleep(random(care_min, care_max)*1000)
        care()
    }
}

function swipe_threads() {
    // 滑动线程
    while (true) {
        log('滑动视频')
        sleep(random(swipe_min, swipe_max)*1000)
        sleep(3000)
        swipe(device.width / 2, device.height - 200, device.width /2, 100, 500)
    }
}

function time_end() {
    
}

function play() {
    init()
    // 启动线程
    threads.start(like_threads);
    threads.start(care_threads);
    threads.start(swipe_threads);
    threads.start(youngWin);
    // threads.start(stuck);
    while (true) {
        if (new Date().getTime()-record_time > running_time*60000) {
            exit()
        }
    }
}

storage = storages.create("meipai");

like_min = storage.get("like_min");
like_max = storage.get("like_max");
care_min = storage.get("care_min");
care_max = storage.get("care_max");
swipe_min = storage.get("swipe_min");
swipe_max = storage.get("swipe_max");
running_time = storage.get("running_time");

auto.waitFor();  // 无障碍服务是否已经启用

record_time = new Date().getTime();


play()