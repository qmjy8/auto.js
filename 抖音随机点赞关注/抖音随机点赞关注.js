/**如果弹出青少年窗口，点击 */
function douYinyoungWin() {
    while (true) {
        if (text("我知道了").exists()) {
            text("我知道了").click();
            log("点击了我知道了(青少年窗口)");
        };
    }
}

function kill(packageName){
    log('杀掉进程')
    // 关闭app
    for (let index = 0; index < 6; index++) {
        if (currentPackage() == packageName){
            back();
            log(currentPackage())
            if(currentPackage() != packageName){
                return true
            }
            sleep(300)
        }
    }
}

function r_tap(uiobj){
    try {
        //toast([uiobj.bounds().centerX(), uiobj.bounds().centerY()])
        //Tap(uiobj.bounds().centerX(), uiobj.bounds().centerY());
        click(uiobj.bounds().centerX(), uiobj.bounds().centerY());
    } catch (error) {
        log(error)
    }
}

// 打开评论
function click_comment(){
    var buttons = descStartsWith('评论').visibleToUser(true).find()
    for (let index = 0; index < buttons.length; index++) {
        var button = buttons[index];
        if(button.bounds().centerY() < device.height && button.bounds().centerY()>0){
            if (textStartsWith('留下你的精彩评论吧').findOnce()==null) {
                log('打开评论')
                r_tap(button)
                sleep(3000);
                if (textStartsWith('评论并转发').findOnce()!==null) {
                    back()
                    sleep(3000);
                    exit_comment()
                }
            }
        }  
    }
}

// 退出评论
function exit_comment() {
    if (textStartsWith('留下你的精彩评论吧').findOnce()!==null) {
        log('关闭评论')
        id("ks").className("android.widget.ImageView").clickable(true).depth(6).findOne().click();
    }
}

// 评论翻页
function page() {
    if (className('androidx.recyclerview.widget.RecyclerView').findOnce()!=null) {
        log('评论翻页')
        className('androidx.recyclerview.widget.RecyclerView').scrollForward();
        sleep(1000);
    }
}

// 评论点赞
function praise_comment() {
    // 心型图片
    praise = className('android.widget.ImageView').id('cd1').selected(false).visibleToUser(true).find();
    //log(praise.length)
    if (praise.length>0) {
        praise_position = praise[random(0, praise.length-1)].bounds();
        log('评论点赞')
        click(praise_position.centerX(), praise_position.centerY());
    }
}

// 评论关注
function my_follow() {
    // 用户头像
    follow = className('android.widget.FrameLayout').id('bcd').selected(false).visibleToUser(true).find();
    //log(follow.length)
    if (follow.length>0) {
        follow_position = follow[random(0, follow.length-1)].bounds();
        log('用户页面')
        click(follow_position.centerX(), follow_position.centerY());
        sleep(3000)
        follow_button = textStartsWith('#  关注').findOnce()
        //log(follow_button)
        if (follow_button!=null) {
            log('关注用户')
            follow_button.click()
        }
        sleep(3000)

        for (let index = 0; index < 7; index++) {
            if (textStartsWith('留下你的精彩评论吧').findOnce()==null) {
                log('退出个人页面')
                descStartsWith('返回').findOnce().click()
                //back()
                sleep(5000)
            }
        }
    }
}

// 私信用户
function my_letter() {
    // 用户头像
    follow = className('android.widget.FrameLayout').id('bcd').selected(false).visibleToUser(true).find();
    //log(follow.length)
    if (follow.length>0) {
        follow_position = follow[random(0, follow.length-1)].bounds();
        log('用户页面')
        click(follow_position.centerX(), follow_position.centerY());
        sleep(3000)
        follow_button = descStartsWith('更多').findOnce()
        //log(follow_button)
        if (follow_button!=null) {
            log('私信用户')
            follow_button.click()
            sleep(3000)
            if (textStartsWith('发私信').findOnce()!=null) {
                textStartsWith('发私信').findOnce().click()
                sleep(3000)
                index = random(0,(my_content_list.length-1)); 
                my_content_a = my_content_list[index]
                log('输入话术')
                setText(my_content_a)
                sleep(3000)
                descStartsWith('发送').findOne().click()
            }
        }
        sleep(3000)

        for (let index = 0; index < 7; index++) {
            if (textStartsWith('留下你的精彩评论吧').findOnce()==null) {
                log('退出个人页面')
                descStartsWith('返回').findOnce().click()
                //back()
                sleep(5000)
            }
        }
    }
}

function init() {
    log('启动抖音')
    // 启动抖音
    app.startActivity({
        action: "android.intent.action.MAIN",
        packageName: "com.ss.android.ugc.aweme",
        className: "com.ss.android.ugc.aweme.splash.SplashActivity",
        category: ["android.intent.category.LAUNCHER"],
        flags: ["activity_new_task"]
    });
    log('定位首页')
    for (let index = 0; index < 7; index++) {
        if (textStartsWith("首页").findOnce()!=null) {
            r_tap(textStartsWith("首页").findOne())
            return
        }else{
            back()
            sleep(3000)
        }
    }
    sleep(3000)
    if (textStartsWith("首页").findOnce()=null) {
        kill("com.ss.android.ugc.aweme")
        sleep(3000)
        init()
    }
}

function swipe_threads() {
    // 滑动视频线程
    while (true) {
        log('滑动视频')
        sleep(random(swipe_min, swipe_max)*1000)
        sleep(3000)
        swipe(device.width / 2, device.height - 200, device.width /2, 100, 100)
    }
}

function click_comment_threads() {
    // 开关评论线程
    while (true) {
        sleep(random(click_comment_min, click_comment_max)*1000)
        click_comment()
        sleep(random(click_comment_min, click_comment_max)*1000)
        exit_comment()
    }
}

function page_threads() {
    // 评论翻页线程
    while (true) {
        sleep(random(page_min, page_max)*1000)
        page()
    }
}

function praise_comment_threads() {
    // 评论点赞线程
    while (true) {
        sleep(random(praise_comment_min, praise_comment_max)*1000)
        praise_comment()
    }
}

function my_follow_threads() {
    // 关注线程
    while (true) {
        sleep(random(my_follow_min, my_follow_max)*1000)
        my_follow()
    }
}

function my_letter_threads() {
    // 私信线程
    while (true) {
        sleep(random(my_letter_min, my_letter_max)*1000)
        my_letter()
    }
}

function my_run_threads() {
    // 状态线程
    while (true) {
        sleep(5000)
        toast('运行ing')
    }
}

function play() {
    init()
    // 启动线程
    threads.start(swipe_threads);
    threads.start(click_comment_threads);
    threads.start(page_threads);
    threads.start(praise_comment_threads);
    threads.start(my_follow_threads);
    threads.start(my_letter_threads);
    threads.start(my_run_threads);
}

storage = storages.create("douyin");

swipe_min = storage.get("swipe_min");
swipe_max = storage.get("swipe_max");
click_comment_min = storage.get("click_comment_min");
click_comment_max = storage.get("click_comment_max");
page_min = storage.get("page_min");
page_max = storage.get("page_max");
praise_comment_min = storage.get("praise_comment_min");
praise_comment_max = storage.get("praise_comment_max");
my_follow_min = storage.get("my_follow_min");
my_follow_max = storage.get("my_follow_max");
my_letter_min = storage.get("my_letter_min");
my_letter_max = storage.get("my_letter_max");
my_content = storage.get("my_content");
my_content_list = my_content.split("|")

auto.waitFor();  // 无障碍服务是否已经启用

play()