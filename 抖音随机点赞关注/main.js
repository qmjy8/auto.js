"ui";
var storage = storages.create("douyin");

var swipe_min = storage.get('swipe_min');
if (swipe_min == undefined) {
    swipe_min = '1'
}

var swipe_max = storage.get('swipe_max');
if (swipe_max == undefined) {
    swipe_max = '10'
}

var click_comment_min = storage.get('click_comment_min');
if (click_comment_min == undefined) {
    click_comment_min = '10'
}

var click_comment_max = storage.get('click_comment_max');
if (click_comment_max == undefined) {
    click_comment_max = '30'
}

var page_min = storage.get('page_min');
if (page_min == undefined) {
    page_min = '1'
}

var page_max = storage.get('page_max');
if (page_max == undefined) {
    page_max = '10'
}

var praise_comment_min = storage.get('praise_comment_min');
if (praise_comment_min == undefined) {
    praise_comment_min = '1'
}

var praise_comment_max = storage.get('praise_comment_max');
if (praise_comment_max == undefined) {
    praise_comment_max = '10'
}

var my_follow_min = storage.get('my_follow_min');
if (my_follow_min == undefined) {
    my_follow_min = '1'
}

var my_follow_max = storage.get('my_follow_max');
if (my_follow_max == undefined) {
    my_follow_max = '10'
}

var my_letter_min = storage.get('my_letter_min');
if (my_letter_min == undefined) {
    my_letter_min = '1'
}

var my_letter_max = storage.get('my_letter_max');
if (my_letter_max == undefined) {
    my_letter_max = '10'
}

var my_content = storage.get('my_content');
if (my_content == undefined) {
    my_content = '话术a|话术b|话术c|话术d'
}

xml ='<vertical padding="16">\
        <button id="ok" text="开始"/>\
        <text text="音量上键停止运行" textSize="40sp"  gravity="center"/>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="滑动视频"/>\
        <input id="swipe_min" inputType="number" text="' + swipe_min + '"/>\
        <text textSize="16sp" textColor="black" text="—"/>\
        <input id="swipe_max" inputType="number" text="' + swipe_max + '"/>\
        <text textSize="16sp" textColor="black" text="/秒"/>\
        </horizontal>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="评论时长"/>\
        <input id="click_comment_min" inputType="number" text="' + click_comment_min + '"/>\
        <text textSize="16sp" textColor="black" text="—"/>\
        <input id="click_comment_max" inputType="number" text="' + click_comment_max + '"/>\
        <text textSize="16sp" textColor="black" text="/秒"/>\
        </horizontal>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="评论翻页"/>\
        <input id="page_min" inputType="number" text="' + page_min + '"/>\
        <text textSize="16sp" textColor="black" text="—"/>\
        <input id="page_max" inputType="number" text="' + page_max + '"/>\
        <text textSize="16sp" textColor="black" text="/秒"/>\
        </horizontal>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="评论点赞"/>\
        <input id="praise_comment_min" inputType="number" text="' + praise_comment_min + '"/>\
        <text textSize="16sp" textColor="black" text="—"/>\
        <input id="praise_comment_max" inputType="number" text="' + praise_comment_max + '"/>\
        <text textSize="16sp" textColor="black" text="/秒"/>\
        </horizontal>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="评论关注"/>\
        <input id="my_follow_min" inputType="number" text="' + my_follow_min + '"/>\
        <text textSize="16sp" textColor="black" text="—"/>\
        <input id="my_follow_max" inputType="number" text="' + my_follow_max + '"/>\
        <text textSize="16sp" textColor="black" text="/秒"/>\
        </horizontal>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="评论私信"/>\
        <input id="my_letter_min" inputType="number" text="' + my_letter_min + '"/>\
        <text textSize="16sp" textColor="black" text="—"/>\
        <input id="my_letter_max" inputType="number" text="' + my_letter_max + '"/>\
        <text textSize="16sp" textColor="black" text="/秒"/>\
        </horizontal>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="私信话术"/>\
        <input id="my_content" text="' + my_content + '"/>\
        </horizontal>\
    </vertical>'

ui.layout(
    xml
);

// 指定确定按钮点击时要执行的动作
ui.ok.click(function(){
    // 通过getText()获取输入的内容
    // toast("开始辣");
    var swipe_min = ui.swipe_min.text();
    var swipe_max = ui.swipe_max.text();
    var click_comment_min = ui.click_comment_min.text();
    var click_comment_max = ui.click_comment_max.text();
    var page_min = ui.page_min.text();
    var page_max = ui.page_max.text();
    var praise_comment_min = ui.praise_comment_min.text();
    var praise_comment_max = ui.praise_comment_max.text();
    var my_follow_min = ui.my_follow_min.text();
    var my_follow_max = ui.my_follow_max.text();
    var my_letter_min = ui.my_letter_min.text();
    var my_letter_max = ui.my_letter_max.text();
    var my_content = ui.my_content.text();
    
    storage.put("swipe_min", parseInt(swipe_min));
    storage.put("swipe_max", parseInt(swipe_max));
    storage.put("click_comment_min", parseInt(click_comment_min));
    storage.put("click_comment_max", parseInt(click_comment_max));
    storage.put("page_min", parseInt(page_min));
    storage.put("page_max", parseInt(page_max));
    storage.put("praise_comment_min", parseInt(praise_comment_min));
    storage.put("praise_comment_max", parseInt(praise_comment_max));
    storage.put("my_follow_min", parseInt(my_follow_min));
    storage.put("my_follow_max", parseInt(my_follow_max));
    storage.put("my_letter_min", parseInt(my_letter_min));
    storage.put("my_letter_max", parseInt(my_letter_max));
    storage.put("my_content", my_content);
    
    var path = '抖音随机点赞关注.js'
    execution = engines.execScriptFile(path);
});
