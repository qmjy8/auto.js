
// 截图
function rootGetScreen() {
    shell("screencap -p /sdcard/code_all.png", true)
}

//获取验证码图片
function get_code_img(){
    toastLog('开始截图')
    log('开始截图')
    rootGetScreen()
    sleep(2000)
    var src = images.read("/sdcard/code_all.png");
    var localtion = className('android.widget.ImageView').clickable(true).findOne(10*1000).bounds()
    var clip = images.clip(src, localtion.left, localtion.top, localtion.width(), localtion.height());
    images.save(clip, "/sdcard/code.png");
    src.recycle()
    clip.recycle()
    log('获取图片成功')
    toastLog('获取图片成功');
}

//超级鹰模块
//////////////////////////////////////////////////////////////////////////////////////////////
//图片的base64
function img64(imgFile){
    let img=images.read(imgFile)
    let img64=images.toBase64(img)
    return img64
}

//获取验证码
function get_tp_code(){
    var res = http.post("http://upload.chaojiying.net/Upload/Processing.php", 
    {
        user: tp_user,
        pass: tp_pwd,
        softid: softid,
        codetype: '1006',
        file_base64: img64(img)
    }
    );
    var result = JSON.parse(res.body.string())
    if(result['err_no'] == 0){
    return result["pic_str"]
    }
    log(result)
    return false 
}

// 输入验证码
function my_captcha() {
    // 获取验证码图片
    log('开始打码')
    get_code_img()
    // 获取验证码
    var code = false
    for (let index = 0; index < 10; index++) {
        code = get_tp_code()
        if (code) {
            break
        }else{
            sleep(10*000)
        }
    }
    if(!code){
        return false
    }
    log('获取到的验证码:' + code)
    //  输入验证码
    a = className('android.widget.EditText').clickable(true).findOne()
    log('输入验证码')
    a.setText(code)
    sleep(5000);

    // 确认输入验证码
    log('确认验证码')
    if (className('android.widget.TextView').clickable(true).text('确定').exists()) {
        className('android.widget.TextView').clickable(true).text('确定').findOne().click()
        sleep(5000);
    }
}

// 判断验证码成功否
function captcha_anormal() {
    var zz = 0
    while(true){
        if (text('您的操作异常，请输入验证码重试:').exists()) {
            toastLog('检测到验证码')
            my_captcha()
        }else{
            sleep(10*1000)
            if(zz % 10 == 0){
                log('打码线程检测中')
            }
        }
        zz += 1
    }
}


function back() {
    // 返回
    log('返回')
    className('android.widget.TextView').clickable(true).depth(6).findOne().click()
    sleep(1000);
}

function slide() {
    log('滑动列表')
    sleep(1000);
    scrollDown(0)
    sleep(1000);
}

function start() {
    sleep(15000);
    while(true){
        log('取粉丝列表')
        list = className('androidx.recyclerview.widget.RecyclerView').scrollable(true).findOne()
        user = list.find(className('android.view.ViewGroup').clickable(true))
        user.forEach(element => {
            sleep(my_time1*1000)
            log('点击粉丝')
            element.click()
            // 点击私信
            sleep(my_time2*1000)
            log('点击私信')
            className('android.widget.ImageButton').clickable(true).findOne().click()
            sleep(my_time3*1000)
            if (text('你们还没发过私信，说句话吧！').exists()) {
                // 输入私信文字
                className('android.widget.EditText').clickable(true).waitFor()
                index = random(0,(my_content_list.length-1)); 
                my_content_a = my_content_list[index]
                log('输入话术')
                className('android.widget.EditText').clickable(true).findOne().setText(my_content_a)
                // 发送私信文字
                sleep(my_time4*1000)
                className('android.widget.TextView').clickable(true).text('发送').waitFor()
                log('点击发送')
                className('android.widget.TextView').clickable(true).text('发送').findOne().click()
                sleep(my_time5*1000)
                if (text('由于对方的隐私设置，你不能进行该操作。').exists()) {
                    log('隐私设置不能私信')
                    click('确定')
                }
                sleep(my_time5*1000)
                // 判断验证码成功否
                sleep(my_time5*1000)
                back()
                sleep(my_time6*1000)
                back()
                sleep(my_time7*1000)
            }else{
                sleep(my_time5*1000)
                back()
                sleep(my_time6*1000)
                back()
                sleep(my_time7*1000)
            }
            });
            slide()
    }
}


var storage = storages.create("meipai");
// 话术内容
var my_content = storage.get("my_content");
// 用户点击间隔
var my_time1  = storage.get("my_time1");
// 点击私信间隔
var my_time2  = storage.get("my_time2");
// 输入话术间隔
var my_time3  = storage.get("my_time3");
// 点击发送间隔
var my_time4  = storage.get("my_time4");
// 返回到用户主页间隔
var my_time5  = storage.get("my_time5");
// 返回到粉丝列表间隔
var my_time6  = storage.get("my_time6");
// 粉丝列表停留间隔
var my_time7  = storage.get("my_time7");

// 打码账号
var tp_user = storage.get('tp_user');
// 打码密码
var tp_pwd = storage.get('tp_pwd');
// 打码id
var softid = storage.get('softid');


my_content_list = my_content.split("|")

// 图片保存地址
var img = '/sdcard/code.png'


auto.waitFor();  // 无障碍服务是否已经启用

launchApp('美拍');
toast('开始辣')


threads.start(captcha_anormal);

start(tp_user, tp_pwd, softid, img)

/*
var my_time  = 1000
var my_content = '好喜欢你鸭'
*/
/*
// 打码账号
var tp_user = storage.get('398683294');
// 打码密码
var tp_pwd = storage.get('s5201314');
// 打码id
var softid = storage.get('903666');
*/
