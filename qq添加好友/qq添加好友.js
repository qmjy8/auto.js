auto.waitFor();  // 无障碍服务是否已经启用

function kill(packageName){
    log('杀掉QQ进程')
    // 关闭app
    for (let index = 0; index < 6; index++) {
        if (currentPackage() == packageName){
            back();
            log(currentPackage())
            if(currentPackage() != packageName){
                return true
            }
            sleep(300)
        }
    }
}

function r_tap(uiobj){
    // 点击函数
    try {
        //toast([uiobj.bounds().centerX(), uiobj.bounds().centerY()])
        //Tap(uiobj.bounds().centerX(), uiobj.bounds().centerY());
        click(uiobj.bounds().centerX(), uiobj.bounds().centerY());
    } catch (error) {
        log(error)
    }
}

function init() {
    log('启动初始化')
    // 初始化
    log('初始化时间')
    record_time = new Date().getTime();
    // 启动QQ
    log('启动QQ')
    app.startActivity({
        action: "android.intent.action.MAIN",
        packageName: "com.tencent.mobileqq",
        className: "com.tencent.mobileqq.activity.SplashActivity",
        category: ["android.intent.category.LAUNCHER"],
        flags: ["activity_new_task"]
    })
    if (textStartsWith("联系人").exists()) {
        log('进入联系人页面')
        r_tap(textStartsWith("联系人").findOne())
        sleep(3000)
        if (className("android.widget.RelativeLayout").clickable(true).desc("新朋友 按钮").exists()) {
            className("android.widget.RelativeLayout").clickable(true).desc("新朋友 按钮").findOne().click()
            sleep(3000)
        }else if(textStartsWith("新朋友").exists()){
            r_tap(textStartsWith("新朋友").findOne())
            sleep(3000)
        }else{
            kill("com.tencent.mobileqq")
            sleep(3000)
            init()
        }
    }else{
        kill("com.tencent.mobileqq")
        sleep(3000)
        init()
    }
}

function tianjiahaoyou() {
    // 添加页面
    log('初始化时间')
    record_time = new Date().getTime();
    log('进入验证页面')
    if (textStartsWith("风险提示").exists()) {
        log('检测到风险提示')
        // toastLog('检测到风险提示')
        textStartsWith("继续").findOne().click()
    }
    //  log(textStartsWith("添加好友").exists())
    if (textStartsWith("添加好友").exists() && textStartsWith("备注").exists() && textStartsWith("分组").exists()) {
        log('检测验证答案')
        if (textStartsWith("问题").exists()) {
            textStartsWith("取消").findOne().click()
            sleep(1000);
        }else{
            // 填写验证信息
            log('填写验证信息')
            className('android.widget.EditText').clickable(true).find()[0].setText(yzxx)
            sleep(2000);
            log('发送好友请求')
            textStartsWith("发送").findOne().click()
            log('发送好友请求成功')
            sleep(3000);
            log('还能继续')
            /*
            if (textStartsWith("添加失败，请勿频繁操作")) {
                textStartsWith('确定').findOne().click()
                sleep(3000);
                textStartsWith("取消").findOne().click()
                sleep(3000);
            }
            */
            log('还能继续2')
            //className("android.widget.TextView").clickable(true).depth(7).id('ivTitleBtnLeft').findOne().click()
            while (!textStartsWith("新朋友").exists()) {
                back()
                sleep(3000)
            }
            sleep(3000);
            log('运行完毕')
        }
    }else{
        log('添加页面2')
        sleep(3000);
        tianjiahaoyou()
    }
}

function tianjia() {
    // 新朋友页面
    log('进入新朋友页面')
    if (textStartsWith("新朋友").exists()) {
        swipe(device.width / 2, device.height - 200, device.width /2, 100, 500)
        sleep(3000)
        // log('查询添加按钮')
        /*
        if (className('android.widget.Button').clickable(true).text('添加').exists()) {
             tianjia_button = className('android.widget.Button').clickable(true).text('添加').find()
             for (let index = 0; index < tianjia_button.length; index++) {
                 element = tianjia_button[index];
                 log('点击添加按钮')
                 element.click()
                 tianjiahaoyou()
                 num = num+1
                 log("当前已添加次数："+ String(num))
                 toast("当前已添加次数："+ String(num))
                 sleep(jhyjg*1000);        // 添加好友间隔时间
                 //swipe(device.width / 2, device.height - 200, device.width /2, 100, 500)
                 //sleep(2000)
                 log('更新时间')
                 record_time = new Date().getTime();
             }
        */
       if (id("nickname").exists()) {
            user = id("nickname").find()
            my_num = user.length-2
            log(user[my_num].text())
            log('点击添加按钮')
            r_tap(user[my_num])
            className("android.widget.Button").clickable(true).desc('加好友').text('加好友').findOne().click()
            sleep(3000);
            tianjiahaoyou()
            num = num+1
            log("当前已添加次数："+ String(num))
            toast("当前已添加次数："+ String(num))
            //sleep(2000)
            //swipe(device.width / 2, device.height - 200, device.width /2, 100, 500)
            sleep(jhyjg*1000);        // 添加好友间隔时间
            log('更新时间')
            record_time = new Date().getTime();            
        }else{
             kill("com.tencent.mobileqq")
             sleep(3000)
             init()
             tianjia()
        }
    } else {
        kill("com.tencent.mobileqq")
        sleep(3000)
        init()
    }
}


var storage = storages.create("qq");

// 验证信息
var my_content = storage.get("my_content");

// 添加好友间隔
var my_time  = storage.get("my_time");

// 验证信息
yzxx = my_content
// 添加好友间隔时间
jhyjg = my_time
// 已添加人数
num = 0
// 控制台
//console.show()
//log('启动控制台')

function play() {
    log('启动主线程')
    init()
    while (true) {
        tianjia()
    }
}

record_time = new Date().getTime();
var play_t = threads.start(play);


while (true) {
    if (new Date().getTime()-record_time > 120000) {
        log('当前时间差:'+String(new Date().getTime()-record_time))
        log('杀死主线程')
        play_t.interrupt();
        play_t = threads.start(play);
    } else {
        log('尚未卡死，休息休息30s后再检测')
        // log('当前时间差大于120000即为卡死')
        log('当前时间差:'+String(new Date().getTime()-record_time))
        sleep(30000)
    }
}
