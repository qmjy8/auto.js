auto.waitFor();  // 无障碍服务是否已经启用

let w = floaty.rawWindow(
    <vertical bg="#60000000">
        <com.stardust.autojs.core.console.ConsoleView h="*" w="*" id="ConS" margin="10 10 10 -35"/>
    </vertical>
  );
w.setSize(-1, device.height * 0.25);
w.setTouchable(false);
ui.run(() => {
    w.ConS.setConsole(runtime.console);
    //w.ConS.findViewById('org.autojs.autojspro.id/input_container').setVisibility(android.view.View.GONE);
});

// 锁保证了操作的原子性
var lock = threads.lock();

function logger(txt){
    // 日志
    log(txt);
    // toast(txt)
}

function my_focus() {
    // 关注
    //抖音关注
    if (packageName('com.ss.android.ugc.aweme').desc('关注').className('android.widget.Button').clickable(true).visibleToUser(true).findOnce()) {
        packageName('com.ss.android.ugc.aweme').desc('关注').className('android.widget.Button').clickable(true).visibleToUser(true).findOnce().click()
        logger('抖音关注')
    }
    //火山关注
    if (packageName('com.ss.android.ugc.live').text('关注').className('android.widget.TextView').clickable(true).visibleToUser(true).findOnce()) {
        packageName('com.ss.android.ugc.live').text('关注').className('android.widget.TextView').clickable(true).visibleToUser(true).findOnce().click()
        logger('火山关注')
    }
    //快手关注
    if (packageName('com.smile.gifmaker').id('slide_play_right_follow_button').clickable(false).visibleToUser(true).findOnce()) {
        packageName('com.smile.gifmaker').id('slide_play_right_follow_button_frame').clickable(true).visibleToUser(true).findOnce().click()
        logger('快手关注')
    }
}

function like() {
    // 双击点赞
    // 抖音点赞
    if (packageName('com.ss.android.ugc.aweme').descContains("喜欢").className("android.widget.ImageView").visibleToUser(true).findOnce()) {
        logger('抖音点赞')
        click(device.width * 0.5, device.height * 0.5);
        sleep(50)
        click(device.width * 0.5, device.height * 0.5);
    }
    // 火山点赞
    if (packageName('com.ss.android.ugc.live').id('cvg').className("android.widget.ImageView").visibleToUser(true).findOnce()) {
        logger('火山点赞')
        click(device.width * 0.5, device.height * 0.5);
        sleep(50)
        click(device.width * 0.5, device.height * 0.5);
    }
    // 快手点赞
    if (packageName('com.smile.gifmaker').id('like_icon').className("android.view.View").visibleToUser(true).findOnce()) {
        logger('快手点赞')
        click(device.width * 0.5, device.height * 0.5);
        sleep(50)
        click(device.width * 0.5, device.height * 0.5);
    }
}

function my_swipe() {
    // 滑动
    // 抖音滑动
    if (packageName('com.ss.android.ugc.aweme').findOnce()) {
        logger('抖音滑动')
        swipe(device.width * 0.5, device.height * 0.8 , device.width * 0.5, 0, 1000)
    }
    // 火山滑动
    if (packageName('com.ss.android.ugc.live').findOnce()) {
        logger('火山滑动')
        swipe(device.width * 0.5, device.height * 0.8 , device.width * 0.5, 0, 1000)
    }
    // 快手滑动
    if (packageName('com.smile.gifmaker').findOnce()) {
        logger('快手滑动')
        swipe(device.width * 0.5, device.height * 0.8 , device.width * 0.5, 0, 1000)
    }
}

/////////////////////线程///////////////////////

function like_threads() {
    // 双击点赞线程
    while (true) {
        sleep(random(like_min, like_max)*1000)
        sleep(3000)
        lock.lock();
        //logger('点赞线程加锁')
        try {
            like()
        } catch (error) {
            logger('点赞线程崩溃')
        }
        lock.unlock();
        //logger('点赞线程解锁')
        sleep(2000)
    }
}

function my_focus_threads() {
    // 关注线程
    while (true) {
        sleep(random(my_focus_min, my_focus_max)*1000)
        sleep(3000)
        lock.lock();
        //logger('关注线程加锁')
        try {
            my_focus()
        } catch (error) {
            logger('关注线程崩溃')
        }
        lock.unlock();
        //logger('关注线程解锁')
        sleep(2000)
    }
}

function swipe_threads() {
    // 滑动视频线程
    while (true) {
        sleep(random(swipe_min, swipe_max)*1000)
        sleep(3000)
        lock.lock();
        //logger('滑动线程加锁')
        try {
            my_swipe()
        } catch (error) {
            logger('滑动线程崩溃')
        }
        lock.unlock();
        //logger('滑动线程解锁')
        sleep(2000)
    }
}

function my_run_threads() {
    // 状态线程
    while (true) {
        sleep(10000)
        lock.lock();
        //logger('状态线程加锁')
        try {
            logger('运行ing（音量+停止）')
        } catch (error) {
            logger('状态线程崩溃')
        }
        lock.unlock();
        //logger('状态线程解锁')
        sleep(2000)
    }
}

function play() {
    logger("请在15秒内打开APP");
    sleep(15000)
    // 启动线程
    // threads.start(my_run_threads);
    threads.start(swipe_threads);
    threads.start(my_focus_threads);
    threads.start(like_threads);
}

storage = storages.create("sanheyi");

swipe_min = storage.get("swipe_min");
swipe_max = storage.get("swipe_max");
my_focus_min = storage.get("my_focus_min");
my_focus_max = storage.get("my_focus_max");
like_min = storage.get("like_min");
like_max = storage.get("like_max");

play()
