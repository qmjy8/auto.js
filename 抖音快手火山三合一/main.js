"ui";
var storage = storages.create("sanheyi");

if (storage.get('my_focus_min') == undefined) {
    storage.put("my_focus_min", '')
}
if (storage.get('my_focus_max') == undefined) {
    storage.put("my_focus_max", '')
}
if (storage.get('like_min') == undefined) {
    storage.put("like_min", '')
}
if (storage.get('like_max') == undefined) {
    storage.put("like_max", '')
}
if (storage.get('swipe_min') == undefined) {
    storage.put("swipe_min", '')
}
if (storage.get('swipe_max') == undefined) {
    storage.put("swipe_max", '')
}
var execution = false
ui.layout(
    <vertical padding="16">\
        <Switch id="autoService" text="无障碍服务(必须)" checked="{{auto.service != null}}" padding="8 8 8 8" textSize="13sp"/>\
        <button id="ok" text="开始（音量+停止）"/>\
        <button id="stop" text="结束脚本"/>
        <button id="float" text="设置悬浮权限"/>
        <button id="log" text="查看日志"/>
        <horizontal>\
        </horizontal>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="关注"/>\
        <input id="my_focus_min" inputType="number" text="{{storage.get('my_focus_min')}}"/>\
        <text textSize="16sp" textColor="black" text="—"/>\
        <input id="my_focus_max" inputType="number" text="{{storage.get('my_focus_max')}}"/>\
        <text textSize="16sp" textColor="black" text="/秒"/>\
        </horizontal>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="点赞"/>\
        <input id="like_min" inputType="number" text="{{storage.get('like_min')}}"/>\
        <text textSize="16sp" textColor="black" text="—"/>\
        <input id="like_max" inputType="number" text="{{storage.get('like_max')}}"/>\
        <text textSize="16sp" textColor="black" text="/秒"/>\
        </horizontal>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="滑动"/>\
        <input id="swipe_min" inputType="number" text="{{storage.get('swipe_min')}}"/>\
        <text textSize="16sp" textColor="black" text="—"/>\
        <input id="swipe_max" inputType="number" text="{{storage.get('swipe_max')}}"/>\
        <text textSize="16sp" textColor="black" text="/秒"/>\
        </horizontal>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="使用说明：（1）开启无障碍服务（2）设置关注点赞滑动间隔（3）开始后请15秒内打开抖音快手火山任意APP"/>\
        </horizontal>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="支持版本：抖音14.7.1-快手9.0.50-火山11.0.0"/>\
        </horizontal>\
    </vertical>
);

// 无障碍权限
ui.autoService.on("check", function(checked) {
    // 用户勾选无障碍服务的选项时，跳转到页面让用户去开启
    if(checked && auto.service == null) {
        app.startActivity({
            action: "android.settings.ACCESSIBILITY_SETTINGS"
        });
    }
    if(!checked && auto.service != null){
        auto.service.disableSelf();
    }
});

ui.emitter.on("resume", function() {
    // 此时根据无障碍服务的开启情况，同步开关的状态
    ui.autoService.checked = auto.service != null;
});


// 开始
ui.ok.click(function(){
    // 通过getText()获取输入的内容
    var my_focus_min = ui.my_focus_min.text();
    var my_focus_max = ui.my_focus_max.text();
    var like_min = ui.like_min.text();
    var like_max = ui.like_max.text();
    var swipe_min = ui.swipe_min.text();
    var swipe_max = ui.swipe_max.text();
    
    storage.put("my_focus_min", parseInt(my_focus_min));
    storage.put("my_focus_max", parseInt(my_focus_max));
    storage.put("like_min", parseInt(like_min));
    storage.put("like_max", parseInt(like_max));
    storage.put("swipe_min", parseInt(swipe_min));
    storage.put("swipe_max", parseInt(swipe_max));

    var path = '抖音快手火山三合一.js'
    execution = engines.execScriptFile(path);
});

//点击停止
ui.stop.click( function() {
    log('脚本结束')
    if(execution){
        execution.getEngine().forceStop()
    }
});

// 悬浮窗权限
ui.float.click( function() {
    var intent = new Intent();
    intent.setAction("android.settings.action.MANAGE_OVERLAY_PERMISSION");
    app.startActivity(intent);
});

// 查看日志
ui.log.click( function() {
    app.startActivity('console')
});