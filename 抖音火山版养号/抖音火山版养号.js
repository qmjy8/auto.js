
/**如果弹出青少年窗口，点击 */
function douYinyoungWin() {
    while (true) {
        if (text("我知道了").exists()) {
            youngWin = text("我知道了")
            console.log("点击了我知道了(青少年窗口)");
            youngWin.click();
        };
    }
}


function logger(txt){
    toastLog(txt)
    //console.trace(txt);
    //toast(txt)
}

//kill抖音
function kill_app(){
    logger('kill APP')
    recents()
    sleep(2000)
    if(className("android.widget.ScrollView").findOne(10)){
        toast(className("android.widget.ScrollView").findOne().child(1).desc())
        if(className("android.widget.ScrollView").findOne().child(1).desc().indexOf('抖音火山版') >=0){
            swipe(device.width/2 - 200, device.height/2 - 200, 0, device.height/2 -200, 10)
        }
    }
    home()
    sleep(2000)
}

function r_tap(uiobj){
    // 点击函数
    try {
        //toast([uiobj.bounds().centerX(), uiobj.bounds().centerY()])
        //Tap(uiobj.bounds().centerX(), uiobj.bounds().centerY());
        click(uiobj.bounds().centerX(), uiobj.bounds().centerY());
    } catch (error) {
        log(error)
    }
}

function like() {
    log('点赞')
    // 点赞
    if (descContains("喜欢").className("android.widget.LinearLayout").visibleToUser().exists()) {
        let btn = descContains("喜欢").className("android.widget.LinearLayout").visibleToUser().findOnce()
        btn.click()
    } else {
        return;
    }
}

function care() {
    log('关注')
    // 关注
    if (textMatches("关注").visibleToUser().exists()) {
        let btn = textMatches("关注").visibleToUser().findOnce()
        btn.click()
    } else {
        return;
    }
}

function judge_home() {
    // 是否进入首页
    for (let index = 0; index < 7; index++) {
        if (!textStartsWith("视频").exists()) {
            back()
            sleep(3000)
        }else{
            sleep(3000)
            return;
        }
    }
    kill("com.ss.android.ugc.live")
    sleep(3000)
    init()
}


// 启动脚本
function init(){
    kill_app()
    logger('打开 APP')
    for (let s = 0; s < 30; s++) {
        var apps = text('抖音火山版').find()
        if(apps.size() == 1){
            apps[0].parent().parent().click()
            sleep(7000)
            logger('打开抖音火山版成功')
            back()
            sleep(4000)
            back()
            // 进入首页
            judge_home()
            if (textStartsWith("视频").exists()) {
                textStartsWith("视频").findOne().parent().click()
                sleep(3000)
                if (descEndsWith("点赞").exists()) {
                    log('随便点一个视频')
                    // r_tap(descEndsWith("点赞").findOne())
                    className('android.widget.ImageView').clickable(true).findOne().click()
                }
            }else{
                kill("com.ss.android.ugc.live")
                sleep(3000)
                init()
            }
            return true
        }else{
            logger('错误:抖音火山版')
            sleep(1000)
        }
    }
    return false
}

function like_threads() {
    
    // 点赞线程
    while (true) {
        sleep(random(like_min, like_max)*1000)
        like()
    }
}

function care_threads() {
    // 关注线程
    while (true) {
        sleep(random(care_min, care_max)*1000)
        care()
    }
}

function swipe_threads() {
    // 滑动线程
    while (true) {
        log('滑动视频')
        sleep(random(swipe_min, swipe_max)*1000)
        sleep(3000)
        swipe(653, 845, 360, 24, 30)
    }
}
/*
function stuck() {
    // 防卡死
    while (true) {
        while (!textStartsWith("首页").exists()) {
            back()
            sleep(3000)
        }
        if (textStartsWith("首页").exists()) {
            r_tap(textStartsWith("首页").findOne())
        }else{
            kill("com.ss.android.ugc.aweme")
            sleep(3000)
            init()
        }
    }
}
*/
function time_end() {
    
}

function play() {
    init()
    // 启动线程
    threads.start(like_threads);
    threads.start(care_threads);
    threads.start(swipe_threads);
    threads.start(douYinyoungWin);
    // threads.start(stuck);
    while (true) {
        if (new Date().getTime()-record_time > running_time*60000) {
            exit()
        }
    }
}

storage = storages.create("douyin_huoshanban");

like_min = storage.get("like_min");
like_max = storage.get("like_max");
care_min = storage.get("care_min");
care_max = storage.get("care_max");
swipe_min = storage.get("swipe_min");
swipe_max = storage.get("swipe_max");
running_time = storage.get("running_time");

auto.waitFor();  // 无障碍服务是否已经启用
console.show()  // 控制台
record_time = new Date().getTime();


play()