"ui";
var storage = storages.create("laiyuewoba");


var name_list = storage.get('name_list');
if (name_list == undefined) {
    name_list = '请按照格式输入昵称,逗号分隔'
}

var account_list = storage.get('account_list');
if (account_list == undefined) {
    account_list = '请按照格式输入账密,逗号分隔'
}

xml ='<vertical padding="16">\
        <button id="ok" text="开始"/>\
        <text text="音量上键停止运行" textSize="40sp"  gravity="center"/>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="昵称列表："/>\
        <input id="name_list" text="' + name_list + '"/>\
        </horizontal>\
        <text textSize="16sp" textColor="black" text="输入昵称格式例如："/>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="张三,李四,王五,赵六"/>\
        </horizontal>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="账密列表："/>\
        <input id="account_list" text="' + account_list + '"/>\
        </horizontal>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="输入账密格式例如："/>\
        </horizontal>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="账号----密码,账号----密码,账号----密码"/>\
        </horizontal>\
    </vertical>'

ui.layout(
    xml
);

// 指定确定按钮点击时要执行的动作
ui.ok.click(function(){
    // 通过getText()获取输入的内容
    // toast("开始辣");
    var name_list = ui.name_list.text();
    var account_list = ui.account_list.text();

    storage.put("name_list", name_list);
    storage.put("account_list", account_list);

    var path = '来约我吧.js'
    execution = engines.execScriptFile(path);
});
