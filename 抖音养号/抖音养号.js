


/**如果弹出青少年窗口，点击 */
function douYinyoungWin() {
    while (true) {
        if (text("我知道了").exists()) {
            youngWin = text("我知道了")
            console.log("点击了我知道了(青少年窗口)");
            youngWin.click();
        };
    }
}

function kill(packageName){
    log('杀掉进程')
    // 关闭app
    for (let index = 0; index < 6; index++) {
        if (currentPackage() == packageName){
            back();
            log(currentPackage())
            if(currentPackage() != packageName){
                return true
            }
            sleep(300)
        }
    }
}


function r_tap(uiobj){
    // 点击函数
    try {
        //toast([uiobj.bounds().centerX(), uiobj.bounds().centerY()])
        //Tap(uiobj.bounds().centerX(), uiobj.bounds().centerY());
        click(uiobj.bounds().centerX(), uiobj.bounds().centerY());
    } catch (error) {
        log(error)
    }
}

function like() {
    log('点赞')
    // 点赞
    if (descContains("喜欢").className("android.widget.ImageView").boundsInside(0, device.height * 0.1, device.width, device.height * 0.9)) {
        let btn = descContains("喜欢").className("android.widget.ImageView").boundsInside(0, device.height * 0.1, device.width, device.height * 0.9).findOnce()
        if (btn){        
            if(!btn.desc().match("已选中"))                    
               btn.parent().click()
        }
    } else {
        return;
    }
}

function care() {
    log('关注')
    // 关注
    if (descMatches("关注").boundsInside(0, device.height * 0.1, device.width, device.height * 0.9).exists()) {
        let btn = descMatches("关注").boundsInside(0, device.height * 0.1, device.width, device.height * 0.9).findOnce()
        if (btn) {
            btn.click()
        }
    } else {
        return;
    }
}

function init() {
    log('启动抖音')
    // 启动抖音
    app.startActivity({
        action: "android.intent.action.MAIN",
        packageName: "com.ss.android.ugc.aweme",
        className: "com.ss.android.ugc.aweme.splash.SplashActivity",
        category: ["android.intent.category.LAUNCHER"],
        flags: ["activity_new_task"]
    });
    while (!textStartsWith("首页").exists()) {
        back()
        sleep(3000)
    }
    if (textStartsWith("首页").exists()) {
        r_tap(textStartsWith("首页").findOne())
    }else{
        kill("com.ss.android.ugc.aweme")
        sleep(3000)
        init()
    }
}

function like_threads() {
    
    // 点赞线程
    while (true) {
        sleep(random(like_min, like_max)*1000)
        like()
    }
}

function care_threads() {
    // 关注线程
    while (true) {
        sleep(random(care_min, care_max)*1000)
        care()
    }
}

function swipe_threads() {
    // 滑动线程
    while (true) {
        log('滑动视频')
        sleep(random(swipe_min, swipe_max)*1000)
        sleep(3000)
        swipe(device.width / 2, device.height - 200, device.width /2, 100, 500)
    }
}
/*
function stuck() {
    // 防卡死
    while (true) {
        while (!textStartsWith("首页").exists()) {
            back()
            sleep(3000)
        }
        if (textStartsWith("首页").exists()) {
            r_tap(textStartsWith("首页").findOne())
        }else{
            kill("com.ss.android.ugc.aweme")
            sleep(3000)
            init()
        }
    }
}
*/
function time_end() {
    
}

function play() {
    init()
    // 启动线程
    threads.start(like_threads);
    threads.start(care_threads);
    threads.start(swipe_threads);
    threads.start(douYinyoungWin);
    // threads.start(stuck);
    while (true) {
        if (new Date().getTime()-record_time > running_time*60000) {
            exit()
        }
    }
}

storage = storages.create("douyin");

like_min = storage.get("like_min");
like_max = storage.get("like_max");
care_min = storage.get("care_min");
care_max = storage.get("care_max");
swipe_min = storage.get("swipe_min");
swipe_max = storage.get("swipe_max");
running_time = storage.get("running_time");

auto.waitFor();  // 无障碍服务是否已经启用

record_time = new Date().getTime();


play()