
function kill(packageName){
    log('杀掉进程')
    // 关闭app
    for (let index = 0; index < 6; index++) {
        if (currentPackage() == packageName){
            back();
            log(currentPackage())
            if(currentPackage() != packageName){
                return true
            }
            sleep(300)
        }
    }
}

function r_tap(uiobj){
    // 点击函数
    try {
        //toast([uiobj.bounds().centerX(), uiobj.bounds().centerY()])
        //Tap(uiobj.bounds().centerX(), uiobj.bounds().centerY());
        click(uiobj.bounds().centerX(), uiobj.bounds().centerY());
    } catch (error) {
        log(error)
    }
}

function sleep_control(control) {
    // 等待控件
    for (let index = 0; index < 7; index++) {
        if (control == null) {
            log('null')
            sleep(3000)
        }else if (control.exists()) {
            log('true')
            return
        }else{
            log('sleep')
            sleep(3000)
        }
    }
    kill("com.soft.blued")
    sleep(3000)
    init()
}


function judge_home() {
    // 是否进入首页
    for (let index = 0; index < 7; index++) {
        if (!className("android.widget.TextView").id('tv_main_find').text('身边').exists()) {
            back()
            sleep(3000)
        }else if(className("android.widget.TextView").id('tv_main_find').text('身边').exists()){
            if (!descContains("Blued").className('android.widget.ImageView').id('iv_main_find').selected(true).exists()) {
                r_tap(className("android.widget.TextView").id('tv_main_find').text('身边').findOnce())
                sleep(3000)
            }
            return;
        }
        sleep(3000)
    }
    kill("com.soft.blued")
    sleep(3000)
    init()
}

function init() {
    log('启动blued')
    // 启动blued
    app.startActivity({
        action: "android.intent.action.MAIN",
        packageName: "com.soft.blued",
        className: "com.soft.blued.icon0",
        category: ["android.intent.category.LAUNCHER"],
        flags: ["activity_new_task"]
    });
    sleep(10000)
    // 是否进入首页
    judge_home()
    log('成功进入首页');
    // 选定分类
    if (!option == '综合') {
        r_tap(textStartsWith(option).findOnce())
        sleep(3000)
    }else if(option == '综合'){
        r_tap(textStartsWith('距离').findOnce())
        sleep(3000)
        r_tap(textStartsWith(option).findOnce())
        sleep(3000)
    }
    while (true) {
        user()
    }
}

function user() {

    // 切换列表
    log('切换列表更新时间')
    record_time = new Date().getTime();   
    sleep(3000)
    if (id('name_view').className('android.widget.TextView').findOnce() == null ){
        id('iv_list_mode').className('android.widget.ImageView').boundsInside(0, device.height * 0.1, device.width, device.height * 0.9).findOnce().click()
        sleep(3000)
    }

    users = id('name_view').className('android.widget.TextView').boundsInside(0, device.height * 0.1, device.width, device.height * 0.9).find()
    for (let index = 1; index < users.length-1; index++) {
        element = users[index];
        element.text()
        log(element.text())
        r_tap(element)
        // 聊天按钮
        log('聊天更新时间')
        record_time = new Date().getTime();   
        sleep(3000)
        className('android.widget.TextView').id('tv_right').text("聊天").waitFor()
        r_tap(className('android.widget.TextView').id('tv_right').text("聊天").findOnce())
        // +按钮
        log('+按钮更新时间')
        record_time = new Date().getTime();   
        sleep(3000)
        className('android.widget.ImageView').id("bt_type_select").waitFor()
        className('android.widget.ImageView').id("bt_type_select").findOnce().click()
        // 照片
        log('照片更新时间')
        record_time = new Date().getTime();   
        sleep(3000)
        textStartsWith("照片").waitFor()
        r_tap(textStartsWith("照片").findOnce())
        // 相册
        log('相册更新时间')
        record_time = new Date().getTime();   
        sleep(3000)
        textStartsWith("相册").waitFor()
        r_tap(textStartsWith("相册").findOnce())
        // 随机选择相片
        log('随机选择相片更新时间')
        record_time = new Date().getTime();   
        sleep(3000)
        className('android.widget.ImageView').desc('Blued').clickable(true).waitFor()
        picture = className('android.widget.ImageView').desc('Blued').clickable(true).find()
        picture[random(0, picture.length-1)].click()
        // 发送相片
        log('发送相片更新时间')
        record_time = new Date().getTime();   
        sleep(3000)
        textStartsWith("发送").waitFor()
        r_tap(textStartsWith("发送").findOnce())
        log('返回首页更新时间')
        record_time = new Date().getTime();   
        // 返回首页
        sleep(3000)
        judge_home()
    };
    sleep(3000)
    swipe(device.width / 2, device.height - 200, device.width /2, 100, 500)
    sleep(3000)
}




auto.waitFor();  // 无障碍服务是否已经启用

storage = storages.create("blued");
option = storage.get("option");

record_time = new Date().getTime();
var play_t = threads.start(init);

while (true) {
    if (new Date().getTime()-record_time > 120000) {
        log('当前时间差:'+String(new Date().getTime()-record_time))
        log('杀死主线程')
        play_t.interrupt();
        record_time = new Date().getTime();   
        sleep(30000)
        play_t = threads.start(init);
    } else {
        log('尚未卡死，休息休息30s后再检测')
        // log('当前时间差大于120000即为卡死')
        log('当前时间差:'+String(new Date().getTime()-record_time))
        sleep(30000)
    }
}
