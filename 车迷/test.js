function r_tap(uiobj){
    try {
        //toast([uiobj.bounds().centerX(), uiobj.bounds().centerY()])
        //Tap(uiobj.bounds().centerX(), uiobj.bounds().centerY());
        click(uiobj.bounds().centerX(), uiobj.bounds().centerY());
    } catch (error) {
        log(error)
    }
}

function Sign_in() {
    // 签到
    if (text('今日签到').visibleToUser(true).exists()) {
        if (text('今日签到').findOnce() != null) {
            text('今日签到').findOnce().parent().click()
            while (true) {
                if(text('网络异常!').exists()){
                    log('网络异常!')
                    sleep(3000)
                    Sign_in()
                }else if(text('已经签到过').exists()){
                    log('已经签到过')
                    return true
                }else if(text('恭喜获得使用券').exists()){
                    log('签到成功')
                    if(text('确定').findOnce() != null){
                        text('确定').findOnce().parent().click()
                    }
                    return true
                }
            }
        }
    }
}

function number_Sign_in() {
    // 账号登陆
    if (text('登录').exists() && text('没有账号？').exists()) {
        number = className("android.widget.EditText").find()
        number[0].setText(my_number)
        number[1].setText(my_password)
        if (text('登录').findOnce() != null) {
            r_tap(text('登录').findOnce())
        }
        while (true) {
            if (text('请输入正确的手机号码!').exists()) {
                log('请输入正确的手机号码!')
                return false
            }else if (text('请输入8-16位登录密码!').exists()){
                log('请输入8-16位登录密码!')
                return false
            }else if (text('密码错误').exists()){
                log('密码错误')
                return false
            }else if (text('密码格式不正确').exists()) {
                log('密码格式不正确')
                return false
            }else if (text('网络异常!').exists()) {
                log('网络异常!')
                sleep(3000)
                number_Sign_in()
            }else if (text('登录成功').exists()) {
                log('登录成功')
                return true
            }

        }
    }
}

function answer() {
    // 获取题目
    txts = className('android.widget.TextView').find()
    start = false
    txt = ''
    for (let x = 0; x < txts.length; x++) {
        if(txts[x].text() == '答题' ){
            start = true
            continue
        }
        if(start){
            if('ABCDEFG'.indexOf(txts[x].text()) >=0){
                txt += txts[x].text()
            }else{
                txt += txts[x].text() + '\n'
            }
        }
    }
    txt = txt.replace('确定', '').trim()

    title = txt.split('\n')[0]
    answers = txt.split('\n')
    answers.splice(0,1)

    log(title)
    log(answers)
    log(text('A正确').exists())
}

function open_answer() {
    // 答题
    my_answer = className('android.view.ViewGroup').clickable(true).enabled(true).focusable(true).indexInParent(3).depth(21).drawingOrder(4).find()
    // log(my_answer.length)
    my_answer[1].click()
    sleep(3000)
    while (true) {
        if(className("android.widget.TextView").text("    (0/0) ").exists()){
            back()
            answer()
        }else{
            log('答题加载完毕')
              break;
          }
    }
    
}
/*
my_number = 13194572590
my_password = 'q1111111'
number_Sign_in()
sleep(3000)
Sign_in()
*/

var Dictionaries_answer = {'Michael':95, 'Bob':75, 'Tracy':85}

log(typeof(Dictionaries_answer))

log(Dictionaries_answer['Michael'])
log(text("D").exists())
if (text("D").findOnce() != null) {
    text("D").findOnce().parent().click()
}
