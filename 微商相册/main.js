"ui";
var storage = storages.create("WSXC");


var time_list = storage.get('time_list');
if (time_list == undefined) {
    time_list = '09:00,11:00'
}



ui.layout(
<vertical padding="16">
        <button id="ok" text="开始（按音量+停止运行）"/>
        <horizontal>
        <text textSize="16sp" textColor="black" text="时间列表："/>
        <input id="time_list" text="{{ time_list }}"/>
        </horizontal>
        <text textSize="16sp" textColor="black" text="格式参考(最少2个)：15:30,18:15,22:30"/>
    </vertical>
);

// 指定确定按钮点击时要执行的动作
ui.ok.click(function(){
    // 通过getText()获取输入的内容
    // toast("开始辣");
    var time_list = ui.time_list.text();
    if(time_list == undefined){
        alert('请输入日期后启动')
    }else{
        storage.put("time_list", time_list.split(','));
        var path = 'run.js'
        execution = engines.execScriptFile(path);
    }

});
