"ui";
var storage = storages.create("kuaishou");


var like_min = storage.get('like_min');
if (like_min == undefined) {
    like_min = '1'
}

var like_max = storage.get('like_max');
if (like_max == undefined) {
    like_max = '10'
}

var care_min = storage.get('care_min');
if (care_min == undefined) {
    care_min = '1'
}

var care_max = storage.get('care_max');
if (care_max == undefined) {
    care_max = '10'
}

var swipe_min = storage.get('swipe_min');
if (swipe_min == undefined) {
    swipe_min = '1'
}

var swipe_max = storage.get('swipe_max');
if (swipe_max == undefined) {
    swipe_max = '10'
}

var running_time = storage.get('running_time');
if (running_time == undefined) {
    running_time = '10'
}

xml ='<vertical padding="16">\
        <button id="ok" text="开始"/>\
        <text text="音量上键停止运行" textSize="40sp"  gravity="center"/>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="点赞"/>\
        <input id="like_min" inputType="number" text="' + like_min + '"/>\
        <text textSize="16sp" textColor="black" text="—"/>\
        <input id="like_max" inputType="number" text="' + like_max + '"/>\
        <text textSize="16sp" textColor="black" text="/秒"/>\
        </horizontal>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="关注"/>\
        <input id="care_min" inputType="number" text="' + care_min + '"/>\
        <text textSize="16sp" textColor="black" text="—"/>\
        <input id="care_max" inputType="number" text="' + care_max + '"/>\
        <text textSize="16sp" textColor="black" text="/秒"/>\
        </horizontal>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="切换视频"/>\
        <input id="swipe_min" inputType="number" text="' + swipe_min + '"/>\
        <text textSize="16sp" textColor="black" text="—"/>\
        <input id="swipe_max" inputType="number" text="' + swipe_max + '"/>\
        <text textSize="16sp" textColor="black" text="/秒"/>\
        </horizontal>\
        <horizontal>\
        <text textSize="16sp" textColor="black" text="运行时长"/>\
        <input id="running_time" inputType="number" text="' + running_time + '"/>\
        <text textSize="16sp" textColor="black" text="/分"/>\
        </horizontal>\
    </vertical>'

ui.layout(
    xml
);

// 指定确定按钮点击时要执行的动作
ui.ok.click(function(){
    // 通过getText()获取输入的内容
    // toast("开始辣");
    var like_min = ui.like_min.text();
    var like_max = ui.like_max.text();
    var care_min = ui.care_min.text();
    var care_max = ui.care_max.text();
    var swipe_min = ui.swipe_min.text();
    var swipe_max = ui.swipe_max.text();
    var running_time = ui.running_time.text();
    
    storage.put("like_min", parseInt(like_min));
    storage.put("like_max", parseInt(like_max));
    storage.put("care_min", parseInt(care_min));
    storage.put("care_max", parseInt(care_max));
    storage.put("swipe_min", parseInt(swipe_min));
    storage.put("swipe_max", parseInt(swipe_max));
    storage.put("running_time", parseInt(running_time));

    var path = '快手养号.js'
    execution = engines.execScriptFile(path);
});
