auto.waitFor();  // 无障碍服务是否已经启用

console.show()

ans_dic= {
    'A': 0,
    'B': 1,
    'C': 2,
    'D': 3,
    'E': 4,
    'F': 5,
    'G': 6
}

function update_time(){
    storage.put('last_time', Date.parse(new  Date())/1000);
}

function logger(txt){
    toastLog(txt)
    //console.trace(txt);
    //toast(txt)
}

function r_tap(uiobj){
    try {
        //toast([uiobj.bounds().centerX(), uiobj.bounds().centerY()])
        //Tap(uiobj.bounds().centerX(), uiobj.bounds().centerY());
        click(uiobj.bounds().centerX(), uiobj.bounds().centerY());
    } catch (error) {
        log(error)
    }
}

// 获取题目
function get_quest() {
    while (true) {
        if(textContains('/5)').exists()){
            break
        }else{
            sleep(1000)
        }
    }
    txts = className('android.widget.TextView').find()
    start = false
    txt = ''
    for (let x = 0; x < txts.length; x++) {
        if(txts[x].text() == '答题' ){
            start = true
            continue
        }
        if(start){
            if('ABCDEFG'.indexOf(txts[x].text()[0]) < 0){
                txt += txts[x].text() + '\n'
            }
        }
    }
    txt = txt.replace('确定', '').trim()

    title = txt.split('\n')[0]
    answers = txt.split('\n')
    answers.splice(0,1)
    title = title.split(')')[1].trim() + '|' +  answers.join("-")
    return [title, answers]
}

//做题，判断题库是否有答案
function do_quest(title, answers){
    log('做题')
    if(tk[title] != undefined){
        log('有答案')
        goods = tk[title]
        for (let x = 0; x < goods.length; x++) {
            good = goods[x];
            text(good).findOne().parent().parent().click()
        }
    }else{
        log('无答案')
        text(answers[0]).findOne().parent().parent().click()
    }
    sleep(1000)
    if(text('确定').exists()){
        text('确定').findOne().parent().click()
    }
}

//匹配正确答案
function get_goods(answers){
    log('获取答案')
    tmp_goods = textStartsWith("答案：").findOne().text().split('：')[1].split('、')
    log(tmp_goods)
    goods = []
    for (let x = 0; x < tmp_goods.length; x++) {
        goods.push(answers[ans_dic[tmp_goods[x]]]) 
    }
    return goods
}

//下一题
function next_quest(){
    log('下一题')
    if(text('下一题').exists()){
        text('下一题').findOne().parent().click()
    }
    for (let x = 0; x < 10; x++) {
        if(text('确定').exists()){
            break
        }else if(text('重新答题').exists()){
            text('重新答题').findOne().parent().click()
        }else{
            sleep(1000)
        } 
    }
}

//保存题库
function save_tk(title, answers){
    log('保存题库')
    goods = get_goods(answers)
    tk[title] = goods
    storage.put('tk', tk)
}

//做一个题
function do_one(){
    result = get_quest()
    title = result[0]
    answers = result[1]
    do_quest(title, answers)
    sleep(2000)
    save_tk(title, answers)
    next_quest()
    sleep(3000)
}


function init(){
    while(true){
        if(currentPackage() == 'com.gzcarmi.oversea'){
            break
        }else{
            log('请打开车秘')
            sleep(3000)
            update_time()
        }
    }
    while(true){
        if(text('我的').exists()){
            break
        }else if(text('请输入您的手机号').exists()){
            log('请登录车秘')
            sleep(3000)
            update_time() 
        }
        else{
            back()
            sleep(5000)
        }
    }
    text('在线答题').findOne().parent().child(3).click()
    sleep(3000)
    log('初始化成功')
}

function jb(){
    init()
    while(true){
        do_one()
        update_time()
        log('当前题库数量:' + Object.keys(tk).length)
    }
}



var storage = storages.create("CM");

var tk = storage.get('tk')
if(tk == undefined){
    tk = {}
    storage.put('tk', tk)
}




var jb_t = threads.start(jb);
var now;
var old;
update_time()



while(true){
    now =  Date.parse(new  Date())/1000;
    old = storage.get('last_time');
    if (now - old > 250){
        logger('卡死检测:检测到卡死，重启任务');
        jb_t.interrupt();
        jb_t = threads.start(jb);
        sleep(2 * 60 * 1000);
    }else{
        logger('卡死检测:运行正常, 检测间隔:' + (now - old));
        sleep(2 * 60 * 1000);
    } 
}