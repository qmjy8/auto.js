
auto.waitFor();  // 无障碍服务是否已经启用
console.show()  // 开启控制台

// 正则匹配 文字头 返回控件
textStartsWith("文字")
// 同理
descStartsWith("文字")
// 正则匹配 文字尾 返回控件
textEndsWith("文字")
// 正则表达式
textMatches(reg)

// 启动app
launchApp('app名字')

// 输入文字
setText("测试")
// 随机
random(1, 5)
// 获取时间
new Date();

// 滑动
swipe(device.width / 2, device.height - 200, device.width /2, 100, 100)
// 轻微滑动
swipe(device.width * 0.6, device.height * 0.8, device.width * 0.6, device.height * 0.6, 1000)
// 慢慢滑动
swipe(device.width * 0.5, device.height * 0.8 , device.width * 0.5, 0, 1000)
// 上滑
scrollUp()
// 下滑
scrollDown()

// 可视
visibleToUser(true)
// 可视2 没有上面的稳定
boundsInside(0, device.height * 0.1, device.width, device.height * 0.9)

// 父类
parent()

function kill(packageName){
    log('杀掉进程')
    // 关闭app
    for (let index = 0; index < 6; index++) {
        if (currentPackage() == packageName){
            back();
            log(currentPackage())
            if(currentPackage() != packageName){
                return true
            }
            sleep(300)
        }
    }
}

function r_tap(uiobj){
    // 取控件坐标点击
    try {
        //toast([uiobj.bounds().centerX(), uiobj.bounds().centerY()])
        //Tap(uiobj.bounds().centerX(), uiobj.bounds().centerY());
        click(uiobj.bounds().centerX(), uiobj.bounds().centerY());
    } catch (error) {
        log(error)
    }
}

function logger(txt){
    // 日志
    log(txt);
    toast(txt)
}

// 无障碍权限ui开关
<Switch id="autoService" text="无障碍服务(必须)" checked="{{auto.service != null}}" padding="8 8 8 8" textSize="13sp"/>
ui.autoService.on("check", function(checked) {
    // 用户勾选无障碍服务的选项时，跳转到页面让用户去开启
    if(checked && auto.service == null) {
        app.startActivity({
            action: "android.settings.ACCESSIBILITY_SETTINGS"
        });
    }
    if(!checked && auto.service != null){
        auto.service.disableSelf();
    }
});

ui.emitter.on("resume", function() {
    // 此时根据无障碍服务的开启情况，同步开关的状态
    ui.autoService.checked = auto.service != null;
});

// 停止脚本按钮
<button layout_weight="1" id="stop" text="结束脚本"/>
ui.stop.click( function() {
    log('脚本结束')

    if(jb_engine){
        jb_engine.getEngine().forceStop()
    }
});

// 悬浮窗权限按钮
<button layout_weight="1" id="float" text="设置悬浮权限"/>
ui.float.click( function() {
    var intent = new Intent();
    intent.setAction("android.settings.action.MANAGE_OVERLAY_PERMISSION");
    app.startActivity(intent);
});

// 日志显示台 需浮窗权限
let w = floaty.rawWindow(
    <vertical bg="#60000000">
        <com.stardust.autojs.core.console.ConsoleView h="*" w="*" id="ConS" margin="10 10 10 -35"/>
    </vertical>
  );
w.setSize(-1, device.height * 0.25);
w.setTouchable(false);
ui.run(() => {
    w.ConS.setConsole(runtime.console);
    //w.ConS.findViewById('org.autojs.autojspro.id/input_container').setVisibility(android.view.View.GONE);
});


// 抖音区

// 抖音通用点赞
function like() {
    log('点赞')
    if (descContains("喜欢").className("android.widget.ImageView").visibleToUser(true).exists()) {
        let btn = descContains("喜欢").className("android.widget.ImageView").visibleToUser(true).findOnce()
        if (btn){        
            if(!btn.desc().match("已选中"))                    
               btn.parent().click()
        }
    } else {
        return;
    }
}

// 抖音通用点击评论
function click_comment(){
    var buttons = descStartsWith('评论').visibleToUser(true).find()
    for (let index = 0; index < buttons.length; index++) {
        var button = buttons[index];
        if(button.bounds().centerY() < device.height && button.bounds().centerY()>0){
            log('打开评论')
            r_tap(button)
            sleep(3000)
            return click_comment_return = true
        }  
    }
    return click_comment_return = false
}

// 抖音通用评论翻页
function page() {
    if (className('androidx.recyclerview.widget.RecyclerView').findOnce()!=null) {
        log('评论翻页')
        className('androidx.recyclerview.widget.RecyclerView').scrollForward();
        sleep(1000);
    }
}

// 抖音通用退出评论 须根据版本更换id
function exit_comment() {
    if (textStartsWith('留下你的精彩评论吧').findOnce()!==null) {
        log('关闭评论')
        id("ks").className("android.widget.ImageView").clickable(true).depth(6).findOne().click();
    }
}

// 抖音通用评论点赞 须根据版本更换id
function praise_comment() {
    // 心型图片
    praise = className('android.widget.ImageView').id('cd1').selected(false).visibleToUser(true).find();
    //log(praise.length)
    if (praise.length>0) {
        // 随机点赞
        praise_position = praise[random(0, praise.length-1)].bounds();
        log('评论点赞')
        click(praise_position.centerX(), praise_position.centerY());
    }
}

// 抖音通用检测评论底部 须根据版本更换id 返回值判断是否继续循环
a = true
while (a) {
    if(text('暂无评论').exists()){
        id("ks").className("android.widget.ImageView").clickable(true).depth(6).findOne().click();
        return a = false
    }else if(text('没有更多了').exists()){
        id("ks").className("android.widget.ImageView").clickable(true).depth(6).findOne().click();
        return a = false
    }else if(text('暂时没有更多了').exists()){
        id("ks").className("android.widget.ImageView").clickable(true).depth(6).findOne().click();
        return a = false
    }else if(text('剩余评论因未授权抖音短视频，暂不支持查看').exists()){
        id("ks").className("android.widget.ImageView").clickable(true).depth(6).findOne().click();
        return a = false
    }
}

// 打码平台
//////////////////////////////////////////////////////////////////////////////////////////////
// 截图
function rootGetScreen() {
    shell("screencap -p /sdcard/code_all.png", true)
}

//获取验证码图片
function get_code_img(){
    toastLog('开始截图')
    log('开始截图')
    rootGetScreen()
    sleep(2000)
    var src = images.read("/sdcard/code_all.png");
    var localtion = className('android.widget.ImageView').clickable(true).findOne(10*1000).bounds()
    var clip = images.clip(src, localtion.left, localtion.top, localtion.width(), localtion.height());
    images.save(clip, "/sdcard/code.png");
    src.recycle()
    clip.recycle()
    log('获取图片成功')
    toastLog('获取图片成功');
}

//超级鹰模块
//////////////////////////////////////////////////////////////////////////////////////////////
//图片的base64
function img64(imgFile){
    let img=images.read(imgFile)
    let img64=images.toBase64(img)
    return img64
}

//获取验证码
function get_tp_code(){
    var res = http.post("http://upload.chaojiying.net/Upload/Processing.php", 
    {
        user: tp_user,
        pass: tp_pwd,
        softid: softid,
        codetype: '1006',
        file_base64: img64(img)
    }
    );
    var result = JSON.parse(res.body.string())
    if(result['err_no'] == 0){
    return result["pic_str"]
    }
    log(result)
    return false 
}

// 输入验证码
function my_captcha() {
    // 获取验证码图片
    log('开始打码')
    get_code_img()
    // 获取验证码
    var code = false
    for (let index = 0; index < 10; index++) {
        code = get_tp_code()
        if (code) {
            break
        }else{
            sleep(10*000)
        }
    }
    if(!code){
        return false
    }
    log('获取到的验证码:' + code)
    //  输入验证码
    a = className('android.widget.EditText').clickable(true).findOne()
    log('输入验证码')
    a.setText(code)
    sleep(5000);

    // 确认输入验证码
    log('确认验证码')
    if (className('android.widget.TextView').clickable(true).text('确定').exists()) {
        className('android.widget.TextView').clickable(true).text('确定').findOne().click()
        sleep(5000);
    }
}

// 判断验证码成功否
function captcha_anormal() {
    var zz = 0
    while(true){
        if (text('您的操作异常，请输入验证码重试:').exists()) {
            toastLog('检测到验证码')
            my_captcha()
        }else{
            sleep(10*1000)
            if(zz % 10 == 0){
                log('打码线程检测中')
            }
        }
        zz += 1
    }
}

// 图片保存地址
var img = '/sdcard/code.png'

threads.start(captcha_anormal);
//////////////////////////////////////////////////////////////////////////////////////////////