// 用户第一个作品点赞
function my_follow() {
    follow = className('android.widget.FrameLayout').id('bcd').selected(false).visibleToUser(true).find();
    //log(follow.length)
    for (let index = 1; index < follow.length; index++) {
        follow_position = follow[index].bounds();
        click(follow_position.centerX(), follow_position.centerY());
        sleep(3000)
        video = descStartsWith('视频1').findOnce()
        //log(video)
        if (video!=null) {
            video.click()
            sleep(3000)
            log('点赞')
            click(like_x, like_y)
            sleep(3000)
        }
        for (let index = 0; index < 7; index++) {
            if (textStartsWith('留下你的精彩评论吧').findOnce()==null) {
                back()
                sleep(3000)
            }
        }
    }
    if(text('暂无评论').exists()){
        id("ks").className("android.widget.ImageView").clickable(true).depth(6).findOne().click();
        return a = false
    }else if(text('没有更多了').exists()){
        id("ks").className("android.widget.ImageView").clickable(true).depth(6).findOne().click();
        return a = false
    }else if(text('暂时没有更多了').exists()){
        id("ks").className("android.widget.ImageView").clickable(true).depth(6).findOne().click();
        return a = false
    }else if(text('剩余评论因未授权抖音短视频，暂不支持查看').exists()){
        id("ks").className("android.widget.ImageView").clickable(true).depth(6).findOne().click();
        return a = false
    }
    sleep(3000)
    page()
    sleep(3000)
}

// 评论翻页
function page() {
    if (className('androidx.recyclerview.widget.RecyclerView').findOnce()!=null) {
        className('androidx.recyclerview.widget.RecyclerView').scrollForward();
        sleep(1000);
    }
}

// 点击评论
function click_comment(){
    var buttons = descStartsWith('评论').visibleToUser(true).find()
    for (let index = 0; index < buttons.length; index++) {
        var button = buttons[index];
        if(button.bounds().centerY() < device.height && button.bounds().centerY()>0){
            log('打开评论')
            r_tap(button)
            sleep(3000)
            return click_comment_return = true
        }  
    }
    return click_comment_return = false
}

function r_tap(uiobj){
    // 点击函数
    try {
        //toast([uiobj.bounds().centerX(), uiobj.bounds().centerY()])
        //Tap(uiobj.bounds().centerX(), uiobj.bounds().centerY());
        click(uiobj.bounds().centerX(), uiobj.bounds().centerY());
    } catch (error) {
        log(error)
    }
}

function like() {
    // 点赞
    if (descContains("喜欢").className("android.widget.ImageView").visibleToUser(true).findOnce() != null) {
        like_x = descContains("喜欢").className("android.widget.ImageView").visibleToUser(true).findOnce().bounds().centerX()
        like_y = descContains("喜欢").className("android.widget.ImageView").visibleToUser(true).findOnce().bounds().centerY()
        // log(like_x, like_y)
        log('点赞定位成功')
        // click(like_x, like_y)
        return like_x, like_y
    }else{
        log('点赞定位失败')
    }
}

function init() {
    log('启动抖音')
    // 启动抖音
    app.startActivity({
        action: "android.intent.action.MAIN",
        packageName: "com.ss.android.ugc.aweme",
        className: "com.ss.android.ugc.aweme.splash.SplashActivity",
        category: ["android.intent.category.LAUNCHER"],
        flags: ["activity_new_task"]
    });
    log('定位首页关注')
    for (let index = 0; index < 7; index++) {
        if (textStartsWith("首页").findOnce()!=null) {
            r_tap(textStartsWith("首页").findOne())
            sleep(3000)
            className('android.widget.TextView').id("gry").text("关注").findOne().click()
            sleep(3000)
            swipe(device.width / 2, device.height - 200, device.width /2, 100, 500)
            sleep(3000)
            like()
            return
        }else{
            back()
            sleep(3000)
        }
    }
    sleep(3000)
    if (textStartsWith("首页").findOnce()=null) {
        kill("com.ss.android.ugc.aweme")
        sleep(3000)
        init()
    }
}

function play() {
    init()
    log('初始化完成')
    while (true) {
        log('进入循环')
        // 点击评论
        click_comment()
        log('打开评论')
        a = true
        while (a) {
            // 用户第一个作品点赞
            my_follow()
        }
        sleep(3000)
        swipe(device.width / 2, device.height - 200, device.width /2, 100, 500)
        sleep(3000)
    }
}

auto.waitFor();  // 无障碍服务是否已经启用

play()